﻿import * as React from 'react'
import { EventsTableComponent } from './EventsTableComponent';
import { EventComponent } from './EventComponent';

export class AppComponent extends React.Component {

    render() {
        return (
            <div style={{
                height: '100vh',
                display: 'grid',
                gridTemplateColumns: 'repeat(12, 1fr)',
                gridTemplateRows: '50% auto'
            }}>
                <div style={{ display: 'grid', gridColumn: '1 / span 12' }}>
                    <EventComponent />
                </div>
                <div style={{ gridColumn: '1 / span 12' }}>
                    <EventsTableComponent evenimente={[{ denumire: '1' }, { denumire: '2' }]} />
                </div>
            </div>
        );
    }

}