﻿import * as React from 'react'
import { Eveniment } from '../Store/Eveniment/Eveniment'

export interface EventsTableProps {
    evenimente: Eveniment[]
}

export class EventsTableComponent extends React.Component<EventsTableProps> {

    constructor(props) {
        super(props);
    }

    onSelectRow() {

    }

    render() {

        let renderEventRow = (eveniment: Eveniment) => {

            return (
                <div key={eveniment.denumire} style={{ gridColumn: '1 / -1' }}>{eveniment.denumire}</div>
            );
        };

        return (
            <div
                style={{
                    display: 'grid',
                    gridTemplateColumns: 'repeat(24, 1fr)'
                }}
            >
                <div>Evenimente inregistrate (TODO)</div>
                {this.props.evenimente.map(eveniment => renderEventRow(eveniment))}
            </div>
        );
    }

}