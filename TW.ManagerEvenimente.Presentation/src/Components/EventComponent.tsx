﻿import * as React from 'react'
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { AppState } from '../Store/AppState';
import { Eveniment } from '../Store/Eveniment/Eveniment';
import { MapComponent, setPushpinLocation, Location, SetPushpinDispatcher } from './MapComponent';

const mapStateToProps = (state: AppState) => ({
    bingMapsApiKey: state.bingMapsApiKey,
    mapCenter: state.mapCenter,
    eveniment: state.selectedEveniment
});
const mapDispatchToProps = (dispatch: Dispatch) => ({
    setPushpinLocation: (location: Location) => dispatch(setPushpinLocation(location))
});

interface EventComponentProps {
    bingMapsApiKey: string,
    mapCenter: number[],
    eveniment: Eveniment,
    setPushpinLocation: SetPushpinDispatcher
}

const EventComponentView: React.FunctionComponent<EventComponentProps> = ({ bingMapsApiKey, mapCenter, eveniment, setPushpinLocation }) => {

    return (
        <>
            <div style={{ gridColumn: '1 / span 6', backgroundColor: '#3498db', textAlign: 'center' }}>
                Zona introducere date (TODO)
            </div>
            <div style={{ gridColumn: '7 / span 6' }}>
                <MapComponent
                    bingMapsApiKey={bingMapsApiKey}
                    center={mapCenter}
                    location={eveniment.position}
                    setPushpinLocation={setPushpinLocation}
                />
            </div>
        </>
    );
}

export const EventComponent = connect(mapStateToProps, mapDispatchToProps)(EventComponentView);