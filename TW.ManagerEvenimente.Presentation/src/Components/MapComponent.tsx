﻿import * as React from 'react'
import { ReactBingmaps } from 'react-bingmaps';
import { SetLocationAction } from '../Store/Eveniment/types';
import { EVT_SET_POSITION } from '../Store/actions'

export function setPushpinLocation(location: Location): SetLocationAction {
    return {
        type: EVT_SET_POSITION,
        payload: [location.latitude, location.longitude]
    };
}

export type SetPushpinDispatcher = (location: number[]) => void;

export interface Location {
    latitude: number,
    longitude: number
}

export interface MapComponentProps {
    bingMapsApiKey: string,
    location: number[],
    center: number[],
    setPushpinLocation: SetPushpinDispatcher
}

export const MapComponent: React.FunctionComponent<MapComponentProps> = ({ bingMapsApiKey, location, center, setPushpinLocation }) => {

    return (
        <ReactBingmaps
            bingmapKey={bingMapsApiKey}
            center={center}
            getLocation={
                { addHandler: "click", callback: setPushpinLocation }
            }
            pushPins={[{
                location,
                option: { color: 'red' }
            }]}
        >
        </ReactBingmaps>
    );
};