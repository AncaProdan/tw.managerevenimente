﻿import { EVT_SET_POSITION } from "../actions";
import { SetLocationAction } from './types'
import { Eveniment } from "./Eveniment";

export const evenimentInitialState: Eveniment = {
    id: '',
    data: '',
    denumire: '',
    position: [44.44247669733852, 26.043658982841325]
};

type EvenimentAction = SetLocationAction;

export function evenimentReducer(state: Eveniment = evenimentInitialState, action: EvenimentAction): Eveniment {

    switch (action.type) {
        case EVT_SET_POSITION:
            return Object.assign({}, state, { position: action.payload });
        default:
            return state;
    }
};