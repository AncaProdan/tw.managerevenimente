﻿import { EVT_SET_POSITION } from '../actions';

export interface SetLocationAction {
    type: typeof EVT_SET_POSITION;
    payload: number[];
}