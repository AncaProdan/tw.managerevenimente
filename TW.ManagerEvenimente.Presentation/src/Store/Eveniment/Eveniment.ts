﻿export interface Eveniment {
    id?: string,
    denumire?: string,
    data?: string,
    position?: number[]
}