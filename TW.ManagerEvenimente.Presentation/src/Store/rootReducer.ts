﻿import { combineReducers } from "redux";
import { evenimentReducer } from './Eveniment/evenimentReducer'

export const rootReducer = combineReducers({
    selectedEveniment: evenimentReducer,
    bingMapsApiKey: () => window.bingMapsApiKey,
    evenimente: () => [],
    mapCenter: () => [44.44247669733852, 26.043658982841325]
} as any);