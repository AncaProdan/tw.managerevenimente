﻿import { Eveniment } from "./Eveniment/Eveniment";

export interface AppState {

    bingMapsApiKey: string;
    mapCenter: number[];
    selectedEveniment: Eveniment;
    evenimente: Eveniment[];

}