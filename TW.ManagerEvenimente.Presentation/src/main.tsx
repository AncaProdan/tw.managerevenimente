﻿import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { createStore } from "redux";
import { Provider } from 'react-redux'
import { AppComponent } from './Components/AppComponent';
import { rootReducer } from './Store/rootReducer'
import './main.scss'

const store = createStore(rootReducer);

ReactDOM.render(
    <Provider store={store}>
        <AppComponent />
    </Provider>, document.getElementById('app'));